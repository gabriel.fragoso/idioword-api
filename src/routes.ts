// -- Third Imports -- //
import { Router } from 'express'

// -- CONTROLLERS -- //
import { CreateUserController } from './controllers/user/CreateUserController'
import { AuthUserController } from './controllers/user/AuthUserController'

// -- MIDDLEWARES -- //
import { isAuthenticated } from './middlewares/isAuthenticated'

const router = Router()

// -- ROTAS USER -- //

  // -- POST -- //
router.post('/register', new CreateUserController().handle)
router.post('/login', new AuthUserController().handle)

export { router }